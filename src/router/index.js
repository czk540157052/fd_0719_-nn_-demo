import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/layout/layout.vue";
import Login from "../views/login/index.vue";
import { getToken } from "@/utils/auth";
Vue.use(VueRouter);
//获取原型对象上的push函数
const originalPush = VueRouter.prototype.push;
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

const routes = [
  {
    path: "/",
    name: "layout",
    component: Home,
    redirect: "IndicatorPanel",
    children: [
      {
        path: "IndicatorPanel",
        component: () => import("../views/IndicatorPanel/index.vue"),
      },
      {
        path: "subCenters",
        component: () => import("../views/subCenters/index.vue"),
      },
      {
        path: "keyProject",
        component: () => import("../views/keyProject/index.vue"),
      },
      {
        path: "cityExperience",
        component: () => import("../views/cityExperience/index.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
];

const router = new VueRouter({
  routes,
});
const whiteList = ["/home", "/login", "/auth-redirect", "/transfer"]; // no redirect whitelist
// router.beforeEach((to, from, next) => {
//   const hasToken = getToken();
//   if (hasToken && whiteList.indexOf(to.path) === -1) {
//     next();
//   } else {
//     if (to.path == "/login") {
//       //如果是登录页面路径，就直接next()
//       next();
//     } else {
//       //不然就跳转到登录；
//       next("/login");
//     }
//   }
// });

export default router;
