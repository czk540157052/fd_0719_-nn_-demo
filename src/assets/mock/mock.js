var mock = {
  // 辐射圈与Ctag自定义标签'@/utils/ctag.js'
  RadiationData: [{
      name: "鑫源国际片区", //自定义标签显示名称
      id: "KeyAreas-鑫源国际片区", //id必填，用于唯一标识和点击事件判断使用
      point: [425136.34375, 2388854, 105.47],
      //辐射圈设置
      Radiation: {
        enable: true, //是否启用(必填)
        radius: 160, //辐射范围
        rippleNumber: 2,
        color: [0.1, 0.8, 0.6, 1], //辐射圈颜色
        brightness: 0.7,
      },
      //自定义标签设置
      CTag: {
        enable: true, //是否启用（必填）
        url: "/addressIcon.html", //自定义标签的HTML页面（必填）
        size: [233, 160], //iframe的大小
        range: [1, 5000], //可视范围
        pivot: [0.5, 0.5],
        offset: [-36, 0, 80], //定位偏移值（x,y,z）用于修正tag位置
        //点击标签后打开的页面(可用于视频播放)
        popup: {
          url: "/int_popup.html",
          size: [0, 0], //大小为0 不可见
        },
      },
    },
    {
      name: "指挥中心片区",
      id: "KeyAreas-指挥中心片区",
      point: [425601.3125, 2390755.75, 75.67443084716797],
      Radiation: {
        enable: true, //是否启用
        radius: 160, //辐射范围
        rippleNumber: 2,
        color: [0.1, 0.8, 0.6, 1], //辐射圈颜色
        brightness: 0.7,
      },
      //自定义标签设置
      CTag: {
        enable: true, //是否启用
        url: "/addressIcon.html", //自定义标签的HTML页面
        size: [233, 160], //iframe的大小
        range: [1, 5000], //可视范围
        pivot: [0.5, 0.5],
        offset: [-36, 0, 80], //定位偏移值（x,y,z）用于修正tag位置
        //点击标签后打开的页面(可用于视频播放)
        popup: {
          url: "/int_popup.html",
          size: [100, 100], //大小为0 不可见
        },
      },
    },
  ],
  // 重点片区-商圈示例点
  BusinessDistrict: [{
    name: "鑫源国际片区", //自定义标签显示名称
    id: "BusinessDistrict-鑫源国际片区", //id必填，用于唯一标识和点击事件判断使用
    point: [425136.34375, 2388854, 105.47],
    //辐射圈设置
    Radiation: {
      enable: false, //是否启用(必填)
    },
    //自定义标签设置
    CTag: {
      enable: true, //是否启用（必填）
      url: "/businessIcon.html", //自定义标签的HTML页面（必填）
      size: [340, 380], //iframe的大小
      range: [1, 5000], //可视范围
      pivot: [0.2, 0.2],
      offset: [-36, 0, 0], //定位偏移值（x,y,z）用于修正tag位置
      //点击标签后打开的页面(可用于视频播放)
      popup: {
        url: "/int_popup.html",
        size: [0, 0], //大小为0 不可见
      },
    },
  }, ]
};

export default mock;