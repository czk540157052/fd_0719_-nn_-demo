const state = {
  IndicatorPanelRetractState: true, //false 为自由操作模式
  IndicatorPanelName: "国土概况",
  MonomerMode: false, //bim单体模式
  monomerData: {}, // 单体信息
  floorData: {}, // 单体楼层信息
  houseData: [],
  // 图层树信息
  infoTree: [],
  datalist: [],
  activeFloor: "", // 选中楼层
  activeHouse: "", // 选中户名
  statusPlaneClip: false,
  onEventMessage: null,
  Vedio: false,
  VedioNunber: 0,
  infoGet: false,
  BIMObject:{},
  BIMShow:false,
  AnalysisBimShow:false,
  BIMObjectonEventMessage:null
};

const mutations = {
  SET_infoGet: (state, infoGet) => {
    state.infoGet = infoGet;
  },
  SET_onEventMessage: (state, onEventMessage) => {
    state.onEventMessage = onEventMessage;
  },

  SET_BIMObjectonEventMessage: (state, BIMObjectonEventMessage) => {
    state.BIMObjectonEventMessage = BIMObjectonEventMessage;
  },

  SET_AnalysisBimShow: (state, AnalysisBimShow) => {
    state.AnalysisBimShow = AnalysisBimShow;
  },

  SET_BIMObject: (state, BIMObject) => {
    state.BIMObject = BIMObject;
  },

  SET_BIMShow: (state, BIMShow) => {
    state.BIMShow = BIMShow;
  },


  SET_Vedio: (state, Vedio) => {
    state.Vedio = Vedio;
  },

  SET_VedioNunber: (state, VedioNunber) => {
    state.VedioNunber = VedioNunber;
  },

  SET_INDICATIORPANEL: (state, type) => {
    state.IndicatorPanelRetractState = type;
  },
  SET_INDICATIORPANELNAME: (state, type) => {
    state.IndicatorPanelName = type;
  },
  SET_MONOMERMODE: (state, type) => {
    state.MonomerMode = type;
  },
  SET_MONOMERDATA: (state, data) => {
    state.monomerData = data;
  },
  SET_FLOORDATA: (state, data) => {
    state.floorData = data;
  },
  SET_HOUSEDATA: (state, data) => {
    state.houseData = data;
  },
  SET_ACTIVEFLOOR: (state, data) => {
    state.activeFloor = data;
  },

  // 图层树信息
  infoTree: (state, data) => {
    state.infoTree = data;
  },

  datalist: (state, data) => {
    state.datalist = data;
  },



  SET_ACTIVEHOUSE: (state, data) => {
    state.activeHouse = data;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
};