const state = {
    onEventMessage: null,
}
const mutations = {
    SET_3DINTERACTIVEMESSAGE: (state, type) => {
        state.onEventMessage = type
    },
}

export default {
    namespaced: true,
    state,
    mutations
}