import axios from 'axios'
import {
  Message,
  MessageBox
} from 'element-ui'
import store from '../store'
import {
  removeToken,
  setToken
} from '@/utils/auth'
import {
  getToken
} from '@/utils/auth'

// 创建axios实例

const service = axios.create({
  baseURL: backstageUrl.interfaceUrl, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 50000 // request timeout
})
// for back-end
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
// service.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    // for user first login to get the menu

    // let each request carry token
    // ['X-Token'] is a custom headers key
    // please modify it according to the actual situation
    config.headers['token'] = getToken()

    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.ackCode && res.ackCode !== 200) {
      Message({
        message: res.message || '请联系管理员！',
        type: 'error',
        duration: 5 * 1000
      })

      // 50008: Illegal token; 50012: Other clients logged in; 40000: Token expired;
      if (res.ackCode === 40000 || res.ackCode === 40011 || res.ackCode === 50012 || res.ackCode === 50008) {
        // to re-login
        // MessageBox.confirm('您已被登出，请重新登录！', '确认登出', {
        //   confirmButtonText: '登出',
        //   cancelButtonText: '取消',
        //   type: 'warning'
        // }).then(() => {
        //   store.dispatch('mix/resetToken').then(() => {
        //     location.reload()
        //   })
        // })
        // store.dispatch('mix/resetToken').then(() => {
        //   location.reload()
        // })
        removeToken()
        location.reload()
      }
      return Promise.reject(new Error(res.message || '请联系管理员！'))
    } else {
      return res
    }
  },
//   error => {
//     console.log('err' + error) // for debug
//     let message = error.message.toLowerCase() === 'network error' ? '当前网络不可用' : error.message
//     Message({
//       message: message,
//       type: 'error',
//       duration: 5 * 1000
//     })
//     return Promise.reject(error)
//   }
)

export default service