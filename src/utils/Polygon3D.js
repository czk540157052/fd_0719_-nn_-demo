export function  addPolygon(polygonList){
      var pldata=[];
         polygonList.forEach(function (value) {
          let type = 1; //3DPolygon的样式
          let o = new Polygon3DData(
            value.id,
            type,
            value.pointSet,
            value.color,
            value.height,
            value.intensity
          );
          pldata.push(o); 
        }); 
         __g.polygon3d.add(pldata);
}