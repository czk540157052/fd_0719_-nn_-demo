export function ODLine(data) {
  for (var i in data) {
    let o = new ODLineData(data[i].id);
    o.color = data[i].color ? data[i].color : Color.Green;
    o.coordinates = data[i].coordinates;
    o.flowRate = data[i].flowRate ? data[i].flowRate : 1;
    o.brightness = data[i].brightness ? data[i].brightness : 10;
    o.bendDegree = data[i].bendDegree ? data[i].bendDegree : 0.5;
    o.tiling = data[i].tiling ? data[i].tiling : 0.5;

    o.lineThickness = data[i].lineThickness ? data[i].lineThickness : 15;
    o.flowPointSizeScale = data[i].flowPointSizeScale
      ? data[i].flowPointSizeScale
      : 30;
    o.labelSizeScale = data[i].labelSizeScale ? data[i].labelSizeScale : 1000;

    o.lineShape = data[i].lineShape ? data[i].lineShape :1; //ODLine模型样式 0:平面 1:柱体，默认值1
    o.lineStyle = data[i].lineStyle ? data[i].lineStyle :0; //ODLine材质样式 0:纯色 1:箭头，2:流动点，默认值0
    o.flowShape = data[i].flowShape ? data[i].flowShape :1; //ODLine发光点样式 0:无 1:球体，默认值0

    o.startPointShape = data[i].startPointShape ? data[i].startPointShape :1;
    o.endPointShape = data[i].endPointShape ? data[i].endPointShape :1;
    o.startLabelShape = data[i].startLabelShape ? data[i].startLabelShape :1;
    o.endLabelShape = data[i].endLabelShape ? data[i].endLabelShape :1;

    __g.odline.add(o);
  }
}
