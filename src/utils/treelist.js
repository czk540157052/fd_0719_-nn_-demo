import Service from "@/api/controller";
let ClearDefultTreelist = [];
export function setLayerState(state) {
  Service.GetLayersTreeList().then((res) => {
    for (var i in res.data) {
      if (
        res.data[i].visible === 'N' 
      ) {
        __g.infoTree.hide(res.data[i].refId);
      }
    }
    if (state === true) {
      for (var i in res.data) {
        if (
          res.data[i].visible === 'Y' 
        ) {
          __g.infoTree.show(res.data[i].refId);
        }
      }
      __g.infoTree.show(["212134484E0C1A9FA21E5C8D8E0F897D"]);
    } else {
      for (var i in res.data) {
        if (
          res.data[i].layerName != "BIM" 
        ) {
          ClearDefultTreelist.push(res.data[i].refId);
        }
      }
      __g.infoTree.hide(ClearDefultTreelist);
      __g.infoTree.hide(["212134484E0C1A9FA21E5C8D8E0F897D"]);
      __g.infoTree.show(['DC17B0F0479EB719889A308E440C9C5F', '0F7252164FD095D95EEA509F9E5E27B1']);
    }
  });
  
}
