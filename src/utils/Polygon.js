//画面方法 *****begin
//获取随机数ID
import geoService from '@/api/geoServer'
let polygonimg = ''
let polygoncolor = ''
let polygonbordercolor = ''
const polygon = {
  getRandomNum() {
    let rdmNum = "";
    for (let i = 0; i < 7; i++) {
      rdmNum += Math.floor(Math.random() * 10); // [0,10)的整数
    }
    return 'T' + rdmNum;
  },
  //遍历数组添加Z轴
  // 参数：
  //   obj  面的数据
  //   tag_obj tag标签数据
  traverse(obj, tag_obj) {
    let z = 10
    for (let a in obj) {
      let arr = Number(a)
      if (Array.isArray(obj[arr]) && Array.isArray(obj[arr + 1])) {
        this.traverse(obj[arr]); //递归遍历
      } else {

        obj[arr].map(item => {
          item.pop()
          item.push(z)
        })
      }
    }
    let color = polygoncolor; //多边形的填充颜色
    let frameColor = polygonbordercolor;
    let frameThickness = 1;
    let id = this.getRandomNum() //随机生成id
    let o = new PolygonData(id, color, obj, frameColor, frameThickness);
    o.depthTest = false;
    //标签的ID，字符串值，也可以用数字（内部会自动转成字符串）

    //坐标值：标签添加的位置

    //图片路径，有3种格式：1）本地路径，2）网络路径，3）BASE64
    let imagePath = ''
    if(polygonimg) {
       imagePath = require('../assets/img/' + polygonimg);
    } 
    // console.log('tupian',imagePath)
    let coord = []
    let text = ""
    let tag_o = ''
    if (tag_obj) {
      let tag_id = this.getRandomNum() //随机生成id;
      coord = [tag_obj.ZXD_X, tag_obj.ZXD_Y, 5];
      text = tag_obj.DJQMC;
      //鼠标点击标签后弹出的网页的URL，也可以是本地视频文件，鼠标点击标签后会弹出视频播放窗口
      let url = '';
      //图片的尺寸
      let imageSize = [28, 28];
      //标签显示的文字

      //标签和文字的可见范围
      let range = [1, 18000];
      let textRange = 18000;
      //标签下方是否显示垂直牵引线
      let showLine = false;
      tag_o = new TagData(tag_id, coord, imagePath, imageSize, url, text, range, showLine);
      //设置文字颜色、背景颜色、
      tag_o.textColor = Color.Black;
      tag_o.textBackgroundColor = Color.White;
      tag_o.textRange = textRange;

    }

    //鼠标悬停时的替换图片
    // tag_o.hoverImagePath = HostConfig.AbsolutePath + '/images/hilightarea.png';
    return {
      o,
      tag_o
    };
  
    
  },
  async addPolygon(val) {
    polygonimg = val.imagePath
    polygoncolor = val.polygoncolor
    polygonbordercolor = val.polygonbordercolor
    let list = []
    let tag_list = []
    const params = {
      service: "WFS",
      version: "1.0.0",
      request: "GetFeature",
      typeName: val.type,
      maxFeatures: 10,
      outputFormat: "application/json",
    }
    const {
      data: res
    } = await geoService.getData(params)
    console.log('面数据', res)
    res.features.forEach(e => {

      let id = this.getRandomNum()
      let {
        o,
        tag_o
      } = this.traverse(e.geometry.coordinates, e.properties, val)
      o.groupId = id;
      tag_o.groupId = id
      list.push(o)
      // this.id_list.push(id)
      if (tag_o) {
        tag_list.push(tag_o)
      }
    })
    __g.polygon.add(list);
    __g.tag.add(tag_list);
  },
  //画面方法 *****End

}
export default polygon