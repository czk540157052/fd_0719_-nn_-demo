export function Polyline_add(data) {
  for (var i in data) {
    let coords = data[i].points;
    let color = data[i].color ? data[i].color : Color.Red;
    let style = data[i].style ? data[i].style : 1;
    let thickness = data[i].thickness ? data[i].thickness : 150;
    let brightness = data[i].brightness ? data[i].brightness : 0.8;
    let flowRate = data[i].flowRate ? data[i].flowRate : 0.5;
    let tiling = -1
    let shape = 1
    let o = new PolylineData(
      data[i].id,
      color,
      coords,
      style,
      thickness,
      brightness,
      flowRate,
      tiling,
      shape
    );
    o.depthTest = false;
    o.groupId = 'Polyline_DLHX'
    __g.polyline.add(o);
  }
}
