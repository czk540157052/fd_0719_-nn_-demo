export function ctag(data) {
  for (var i in data) {
    if (data[i].Radiation.enable == true) {
      let coordinate = data[i].point;
      let radius = data[i].Radiation.radius ? data[i].Radiation.radius : 160;
      let rippleNumber = data[i].Radiation.rippleNumber
        ? data[i].Radiation.rippleNumber
        : 2;
      let color = data[i].Radiation.color
        ? data[i].Radiation.color
        : [0.1, 0.8, 0.6, 1];
      let brightness = data[i].Radiation.brightness
        ? data[i].Radiation.brightness
        : 0.7;
      let o = new RadiationPointData(
        "FSQ_" + data[i].name,
        coordinate,
        radius,
        rippleNumber,
        color,
        brightness
      );
      __g.radiationPoint.add(o);
    }
    if (data[i].CTag.enable === true) {
      let id = data[i].id;
      var datpoint = data[i].point;
      datpoint[0] = datpoint[0] + data[i].CTag.offset[0];
      datpoint[1] = datpoint[1] + data[i].CTag.offset[1];
      datpoint[2] = datpoint[2] + data[i].CTag.offset[2];
      let coord = datpoint;
      let page =
        HostConfig.AbsolutePath + data[i].CTag.url + "?title=" + data[i].name;
      let contentWeb = new WebUIData(
        page,
        data[i].CTag.size[0],
        data[i].CTag.size[1]
      );
      let popupWeb = new WebUIData(
        HostConfig.AbsolutePath + data[i].CTag.popup.url,
        data[i].CTag.popup.size[0],
        data[i].CTag.popup.size[1]
      );
      let pivot = data[i].CTag.pivot ? data[i].CTag.pivot : [0.5, 0.5];
      let range = data[i].CTag.range ? data[i].CTag.range : [1, 5000];
      let otag = new CustomTagData(
        id,
        coord,
        contentWeb,
        popupWeb,
        pivot,
        range
      );
      __g.ctag.add(otag);
    }
  }
}

