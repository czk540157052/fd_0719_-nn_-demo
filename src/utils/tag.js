//示例数据
// var test = [
//   {
//     id: "id值", //点击事件可以获取
//     coord: [1, 1, 1], //坐标x,y,z
//     imagePath: "../../../assets/img/xxx.png", //图标地址
//     size: ["width", "height"], //图标大小
//     range: [1, 8000.0], //可视范围
//   },
// ];
let tagData = [];
export function tag(data) {
  __g.tag.clear(() => {
      for (let i in data) {
        let coord = [data[i].coord[0], data[i].coord[1], data[i].coord[2]]; //热力点的坐标
        {
          //添加热力点定位标签
          let imagePath = require('../assets/img/' + data[i].imagePath);
          tagData.push(
            new TagData(
              data[i].id,
              coord,
              imagePath,
              data[i].size ? data[i].size : [30, 30],
              "",
              "",
              data[i].range ? data[i].range : [1, 8000.0],
              false
            )
          );
        }
      }
      __g.tag.add(tagData);
    }

  )
}