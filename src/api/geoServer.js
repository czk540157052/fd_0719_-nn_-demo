import axios from "axios";
const Qs = require("qs");

const service = axios.create({
  baseURL: backstageUrl.geoServerUrl, // url = base url + request url
  timeout: 20000, // request timeout
});

service.interceptors.request.use(
  (config) => {
    // do something before request is sent
    config.headers["Content-Type"] = "application/x-www-form-urlencoded";
    // for user first login to get the menu
    return config;
  },
  (error) => {
    // do something with request error
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

let geoService = {
  getData(data) {
    return service({
      url: "/geoserver/cite/ows?" + Qs.stringify(data),
      method: "get",
    });
  },
  ClickQuery(data) {
    var precision = 1; //查询精度
    var points = [
        data.point[0] - precision,
        data.point[1] - precision,
        data.point[0] + precision,
        data.point[1] + precision,
    ];
    var data = {
      SERVICE: "WMS",
      VERSION: "1.1.1",
      REQUEST: " GetFeatureInfo",
      FORMAT: "image/png",
      TRANSPARENT: true,
      QUERY_LAYERS: data.layers,
      LAYERS: data.layers,
      exceptions: "application/vnd.ogc.se_inimage",
      INFO_FORMAT: "application/json",
      FEATURE_COUNT: 50,
      X: 50,
      Y: 50,
      SRS: "EPSG:4546",
      WIDTH: 101,
      HEIGHT: 101,
      BBOX: points[0] + "," + points[1] + "," + points[2] + "," + points[3],
    };
    return service({
      url: "/geoserver/cite/ows?" + Qs.stringify(data),
      method: "get",
    });
  },
};

export default geoService;
