import request from '@/utils/request'
const Qs = require('qs')

let BimService = {
  getCimRevitProps(data) {
    return request({
      url: '/CimRevitProps/search?objectId=' + data,
      method: 'get',
    })
  },
  getcomponentsTree(data,buildingName) {
    return request({
      url: '/CimRevitProps/componentsTree?prjIdOrProjName=' + data + '&buildingName='+buildingName,
      method: 'get',
      method: 'get',
    })
  },
  getfloorTree(data,buildingName) {
    return request({
      url: '/CimRevitProps/floorTree?prjIdOrProjName=' + data + '&buildingName='+buildingName,
      method: 'get',
    })
  },
  getbimDetailInfor(data){
    return request({
      url: '/CimRevitProps/search?objectId=' + data,
      method: 'get',
    })
  }
}
export default BimService