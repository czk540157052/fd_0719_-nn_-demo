import request from '@/utils/request'
const Qs = require('qs')

let examinationService = {
  getCimConcession() {
    return request({
      url: '/CimConcessionAnalysis/list',
      method: 'get',
      
    })
  },
}

export default examinationService