import request from '@/utils/request'
const Qs = require('qs')

let analysisService = {
  getCimAuxiliaryTools(data) {
    return request({
      url: '/CimAuxiliaryTools/findByType?type=' + data,
      method: 'get',
    })
  },
  saveCimAuxiliaryTools(data) {
    return request({
      url: '/CimAuxiliaryTools/save',
      method: 'post',
      data: Qs.stringify(data)
    })
  },
  /**
   * 视频监控接口
   */
  saveVideoPoint(data) {
    return request({
      url: '/CimVideoPointInfo/save',
      method: 'post',
      data: Qs.stringify(data)
    })
  },
  deleteVideoPoint(id) {
    return request({
      url: '/CimVideoPointInfo/deleteById/' + id,
      method: 'delete'
    })
  },
  updateVideoPoint(data) {
    return request({
      url: '/CimVideoPointInfo/hasCollection',
      method: 'post',
      data: Qs.stringify(data)
    })
  },
  getVideoPoint(data) {
    return request({
      url: '/CimVideoPointInfo/list?' + data,
      method: 'get'
    })
  },
  /**
   * 分层分户接口
   */
  // 楼层基本信息
  FloorBasicInfo(data) {
    return request({
      url: '/CimFloorBasicInfo/page?' + Qs.stringify(data),
      method: 'get'
    })
  },
  // 楼层的分户数量和居民数量统计
  householdandResidentstatistics(id) {
    return request({
      url: '/CimFloorBasicInfo/householdandResidentstatistics?floor=' + id,
      method: 'get'
    })
  },
  // 分户基本信息
  HouseholdBasicInfo(id) {
    return request({
      url: '/CimHouseholdBasicInfo/list?floor=' + id,
      method: 'get'
    })
  },
  // 居民基本信息
  ResidentInfo(id) {
    return request({
      url: '/CimResidentInfo/list?houseId=' + id,
      method: 'get'
    })
  },
  /**
   * 图层查询接口
   */
  // 根据名称查询图层
  layer2ComponentIds(data) {
    return request({
      url: '/CimRevitProps/layer2ComponentIds?layerName=' + data,
      method: 'get'
    })
  }
}

export default analysisService