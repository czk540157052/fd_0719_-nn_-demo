import request from '@/utils/request'
const Qs = require('qs')

let hotCollectionService = {
  getList(data) {
    return request({
      url: '/CimBookmark/page?'+ Qs.stringify(data),
      method: 'get',
      
    })
  },
  saveHotCollection(data) {
    return request({
      url: '/CimBookmark/',
      method: 'post',
      data: Qs.stringify(data)
    })
  },
  deleteHotCollection(data) {
    return request({
      url: '/CimBookmark/' + data,
      method: 'delete'
    })
  }
}

export default hotCollectionService