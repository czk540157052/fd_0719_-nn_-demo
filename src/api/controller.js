import request from '@/utils/request'
import {
    removeToken,
    setToken
} from '@/utils/auth'
const MD5 = require('md5.js')
const Qs = require('qs')
let md5 = new MD5()
let Service = {
    login() {
        request({
            url: '/Login/login',
            method: 'post',
            data: Qs.stringify({
                appCode: null,
                loginName: backstageUrl.loginForm.username,
                password: md5.update(backstageUrl.loginForm.password).digest('hex')
            })
        }).then(res => {
            removeToken()
            setToken(res.data.token) // 设置token})

        })
    },
    GetLayersList() {
        return request({
            url: '/CimLayers/list',
            method: 'get'
        })
    },
    GetLayersPage(data) {
        return request({
            url: '/CimLayers/page?'+ Qs.stringify(data),
            method: 'get'
        })
    },
    GetLayersTreeList() {
        return request({
            url: '/CimLayers/layersTreeList',
            method: 'get'
        })
    },
    GetLayersID(id) {
        return request({
            url: '/CimLayers/'+id,
            method: 'get'
        })
    },
    getCimprofileImg(data) {
        return request({
            url: '/CimAcpPrj/profile?id=' + data,
            method: 'get',
        })
    },
    getCimprojectList(){
        return request({
            url: '/CimAcpPrj/page',
            method: 'get',
        })
    } 
   
}

export default Service