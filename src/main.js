import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/css/global.less'
import './assets/iconfont/iconfont.css'
import * as echarts from 'echarts'
// import "./assets/css/iconfont.css";




Vue.use(ElementUI);
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts


import axios from 'axios'

const baseURL11 = 'http://127.0.0.1:8080/mock/'
var baseURL1 = axios.create({
  baseURL: baseURL11,
  timeout: 2000,
  headers: {
    'X-Custom-Header': 'foobar'
  }
})
//本地Mock接口
Vue.prototype.$http = baseURL1


//开启云服务配置
// router.beforeEach((to, from, next) => {
//     next()
//     router.push({
//         query: {
//             bsms: 'ms',
//         }
//     })
// })



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')